﻿namespace KLakShared
{
	public record UpdateInfo
	{
		public string          Name     { get; init; }
		public int             Index    { get; init; }
		public EPlayerPosition Position { get; init; }
	};
}
