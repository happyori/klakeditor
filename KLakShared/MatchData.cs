﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

#nullable enable
namespace KLakShared
{
	/// <summary>
	/// Represents a single match data including players' names and the match index
	/// </summary>
	public class MatchData : IDisposable, IEquatable<MatchData>
	{
		private readonly ISubject<string> _newPlayerOneNameSubject;
		private readonly ISubject<string> _newPlayerTwoNameSubject;

		public string PlayerOneName { get; init; }
		public string PlayerTwoName { get; init; }
		public int    Index         { get; init; }

		public string? NewPlayerOneName { get; private set; }

		public string? NewPlayerTwoName { get; private set; }

		public MatchData( int index, string playerOneName, string playerTwoName )
		{
			PlayerOneName = playerOneName;
			PlayerTwoName = playerTwoName;
			Index         = index;

			_newPlayerOneNameSubject = new BehaviorSubject<string>(playerOneName);
			_newPlayerTwoNameSubject = new BehaviorSubject<string>(playerTwoName);
		}

		public void SetPlayerName(EPlayerPosition position, string newName)
		{
			if (position == EPlayerPosition.Player1)
			{
				NewPlayerOneName = newName;
				_newPlayerOneNameSubject.OnNext(newName);
			}
			else
			{
				NewPlayerTwoName = newName;
				_newPlayerTwoNameSubject.OnNext(newName);
			}
		}

		public IObservable<string> PlayerOneNameChanged => _newPlayerOneNameSubject.AsObservable();
		public IObservable<string> PlayerTwoNameChanged => _newPlayerTwoNameSubject.AsObservable();

		public void Dispose( )
		{
			_newPlayerOneNameSubject.OnCompleted();
			_newPlayerTwoNameSubject.OnCompleted();
		}

		public bool Equals(MatchData? other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Index == other.Index;
		}

		public override int GetHashCode( ) { return Index.GetHashCode(); }
	};
}
