﻿# KLaK Editor

---

Kill La Kill Tourney Name Editor

## Basic Information

---

- Max Name size -> 132 Bites (every other byte)
- Initial Offset (from SOF to First name) -> 0x000000D8
- Offset Between Names per Match -> 0xE0
- Offset From First Name to next Match -> 0x2B0
- Offset From Last Name to next Match -> 0x1D0
