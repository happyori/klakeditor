﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using KlaKLib;
using KLakShared;
using KLaKUI.Utility;
using KLaKUI.ViewModels;
using ReactiveUI;

namespace KLaKUI.Views
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow
	{
		public MainWindow( )
		{
			InitializeComponent();
			ViewModel               = new MainWindowViewModel();
			MatchIDCB.ItemsSource   = ViewModel.MatchIDs;
			MatchIDCB.SelectedIndex = 0;
			var updateInfo = this.WhenAnyValue(w => w.NewNameTB.Text, w => w.IsPlayer1RB.IsChecked,
											   w => w.MatchIDCB.SelectedIndex,
											   (name, isPlayer1, id) => new UpdateInfo
											   {
												   Index = id, Name = name, Position =
													   isPlayer1 != null && (bool)isPlayer1
														   ? EPlayerPosition.Player1
														   : EPlayerPosition.Player2
											   });

			this.WhenActivated(d =>
			{
				this.OneWayBind(ViewModel,
						  vm => vm.Items,
						  window => window.MatchInfoList.ItemsSource)
					.DisposeWith(d);
				
				this.Bind(ViewModel,
						  vm => vm.NewName,
						  window => window.NewNameTB.Text)
					.DisposeWith(d);

				this.Bind(ViewModel,
						  vm => vm.SelectedIndex,
						  window => window.MatchIDCB.SelectedIndex)
					.DisposeWith(d);
				
				this.BindCommand(ViewModel,
								 vm => vm.UpdateCommand,
								 window => window.UpdateBtn,
								 updateInfo)
					.DisposeWith(d);
				this.BindCommand(ViewModel,
								 vm => vm.ResetCommand,
								 window => window.ResetBtn)
					.DisposeWith(d);
				this.BindCommand(ViewModel,
								 vm => vm.SaveCommand,
								 window => window.SaveBtn)
					.DisposeWith(d);
			});
		}
	}
}
