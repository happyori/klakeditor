﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Documents;
using System.Windows.Input;
using DynamicData;
using KlaKLib;
using KLakShared;
using KLaKUI.Utility;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace KLaKUI.ViewModels
{
	public class MainWindowViewModel : ReactiveObject, IDisposable
	{
		public readonly KlaK klaKLib;

		private readonly IDisposable _cleanUp;

		public ReactiveCommand<UpdateInfo, Unit> UpdateCommand { get; }
		public ReactiveCommand<Unit, Unit>       ResetCommand  { get; }
		public ReactiveCommand<Unit, Unit>       SaveCommand   { get; }

		private ReadOnlyObservableCollection<MatchDataProxy> _items;

		public ReadOnlyObservableCollection<MatchDataProxy> Items => _items;

		public MainWindowViewModel( )
		{
			MatchIDs = new List<int>(Enumerable.Range(1, KlaK.MAX_REPLAYS));
			klaKLib  = new KlaK();
			UpdateCommand = ReactiveCommand.Create<UpdateInfo>(info =>
			{
				klaKLib.UpdatePlayerName(info.Index,
										 info.Position,
										 info.Name);
			});
			ResetCommand = ReactiveCommand.Create(( ) =>
			{
				NewName       = string.Empty;
				SelectedIndex = 0;
				klaKLib.ResetFile();
			});
			SaveCommand = ReactiveCommand.Create(( ) => { klaKLib.SaveFile(); });

			IDisposable loader = klaKLib.Connect()
										.Transform(data => new MatchDataProxy(data))
										.ObserveOnDispatcher()
										.Bind(out _items)
										.DisposeMany()
										.Subscribe();
			_cleanUp = new CompositeDisposable(loader, klaKLib);
		}

		[Reactive]
		public string? NewName { get; set; }

		[Reactive]
		public int SelectedIndex { get; set; }

		public List<int> MatchIDs { get; }

		public void Dispose( ) { _cleanUp.Dispose(); }
	}
}
