﻿using System;
using System.Reactive.Disposables;
using DynamicData.Binding;
using KLakShared;

namespace KLaKUI.Utility
{
	public class MatchDataProxy : AbstractNotifyPropertyChanged, IDisposable, IEquatable<MatchDataProxy>
	{
		private          int         _id;
		private readonly MatchData   _data;
		private readonly IDisposable _disposable;

		public MatchDataProxy(MatchData data)
		{
			_data = data;
			_id   = data.Index;

			IDisposable nameOneRefresher = data.PlayerOneNameChanged
											   .Subscribe(name => NewPlayerOneName = name);
			IDisposable nameTwoRefresher = data.PlayerTwoNameChanged
											   .Subscribe(name => NewPlayerTwoName = name);

			_disposable = Disposable.Create(( ) =>
			{
				nameOneRefresher.Dispose();
				nameTwoRefresher.Dispose();
			});
		}

		private string? _newPlayerOneName;

		public string NewPlayerOneName
		{
			get => _newPlayerOneName ?? string.Empty;
			set => SetAndRaise(ref _newPlayerOneName, value);
		}

		private string? _newPlayerTwoName;

		public string NewPlayerTwoName
		{
			get => _newPlayerTwoName ?? string.Empty;
			set => SetAndRaise(ref _newPlayerTwoName, value);
		}

		#region Exposing Delegates

		public int Index => _data.Index;

		public string PlayerOneName => _data.PlayerOneName;

		public string PlayerTwoName => _data.PlayerTwoName;

		public int GameIndex => Index + 1;

		#endregion

		public void Dispose( ) { _disposable.Dispose(); }

		public bool Equals(MatchDataProxy? other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return _id == other._id;
		}
	}
}
