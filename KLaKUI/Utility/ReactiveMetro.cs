﻿using System.Windows;
using KLaKUI.ViewModels;
using MahApps.Metro.Controls;
using ReactiveUI;

namespace KLaKUI.Utility
{
	public class ReactiveMetro<TViewModel> : MetroWindow, IViewFor<TViewModel> where TViewModel : class
	{
		public static readonly DependencyProperty ViewModelProperty =
			DependencyProperty.Register(nameof(ViewModel),
										typeof(TViewModel),
										typeof(ReactiveMetro<TViewModel>),
										new PropertyMetadata(null));

		public TViewModel? ViewModel
		{
			get => (TViewModel)GetValue(ViewModelProperty);
			set => SetValue(ViewModelProperty, value);
		}

		object? IViewFor.ViewModel
		{
			get => ViewModel;
			set => ViewModel = value as TViewModel;
		}
		
		
	}
}
