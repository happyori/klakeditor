using System;
using System.Collections.Generic;
using System.IO;
using Bogus;
using FluentAssertions;
using KlaKLib;
using KLakShared;
using Xunit;
using static KLakShared.EPlayerPosition;

namespace KlaKTests
{
	public class LibraryTests : IDisposable
	{
		private       KlaK       _subject;
		private const string     TESTFILE = "testing.bin";
		private       FileStream _testFile;
		private       FileStream _resultTestFile;
		private       string     _testFilePath;
		private       string     _resultTestFilePath;
		private       Faker      _faker;

		public LibraryTests( )
		{
			_faker              = new Faker();
			_subject            = new KlaK(false);
			_testFilePath       = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TESTFILE);
			_resultTestFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TESTFILE + ".result");
			string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
									   "../../../../02_replay_list.bin.template");
			File.Copy(path, _testFilePath,       true);
			File.Copy(path, _resultTestFilePath, true);
			_testFile           = new FileStream(_testFilePath,       FileMode.Open);
			_resultTestFile     = new FileStream(_resultTestFilePath, FileMode.Open);
			_subject.file       = _testFile;
			_subject.resultFile = _resultTestFile;
		}

		public void Dispose( )
		{
			_subject?.Dispose();
			_testFile.Dispose();
			_resultTestFile.Dispose();
		}

		[Fact]
		public void ShouldOpenFile( )
		{
			_testFile.Dispose();
			_subject.file       = null;
			_subject.resultFile = null;

			_subject.replayListName = TESTFILE;
			_subject.Invoking(sub => sub.OpenReplayList())
				   .Should()
				   .NotThrow();
		}

		[Fact]
		public void ShouldAutoOpenFile( )
		{
			_subject.Dispose();

			_subject = new KlaK();
			_subject.file
				   .CanRead
				   .Should()
				   .BeTrue();
		}

		[Theory,
		 InlineData(0, Player1, 0x00D8),
		 InlineData(0, Player2, 0x01B8),
		 InlineData(3, Player1, 0x08E8),
		 InlineData(2, Player2, 0x0718)]
		public void ShouldSeekToPlayerName(int index, EPlayerPosition position, long expected)
		{
			_subject.Invoking(sub => sub.GotoPlayerNames(index, position, _testFile))
				   .Should()
				   .NotThrow();

			_subject.file
				   .Position
				   .Should()
				   .Be(expected);
		}

		[Fact]
		public void ShouldThrowIfNameIsTooLong( )
		{
			string fakeName = _faker.Random.String(90);

			_subject.Invoking(sub => sub.UpdatePlayerName(0, Player1, fakeName))
				   .Should()
				   .Throw<ArgumentException>();
		}

		[Fact]
		public void ShouldThrowIfIndexExceedsMax( )
		{
			_subject.Invoking(sub => sub.GotoPlayerNames(100, Player1, _testFile))
				   .Should()
				   .Throw<ArgumentException>();
		}

		[Theory,
		 InlineData(-1, 40),
		 InlineData(10, 10),
		 InlineData(5,  5),
		 InlineData(50, 40),]
		public void ShouldFindACertainAmountOfPlayerNames(int amount, int expected)
		{
			List<MatchData> results = amount == -1
										  ? _subject.GetPlayerNamesFromStream(_testFile)
										  : _subject.GetPlayerNamesFromStream(_testFile, amount);

			results.Should()
				   .HaveCount(expected);
		}

		[Fact]
		public void ShouldFindPlayerName( )
		{
			var expected = new MatchData(0, "DanieD00", "AtomicOreo");

			List<MatchData> results = _subject.GetPlayerNamesFromStream(_testFile, 2);

			results[0]
			   .PlayerOneName
			   .Should()
			   .BeEquivalentTo(expected.PlayerOneName);
			
			results[0]
			   .PlayerTwoName
			   .Should()
			   .BeEquivalentTo(expected.PlayerTwoName);

			results[0]
			   .Index
			   .Should()
			   .Be(0);
			
			results.Should()
				   .NotContainNulls()
				   .And
				   .HaveCount(2)
				   .And
				   .Contain(expected);
		}

		[Theory,
		 InlineData(0,  Player1, "OhGodIsThatABorgar"),
		 InlineData(5,  Player2, "CoolNameICameUpWith"),
		 InlineData(2,  Player1, "Pistachio"),
		 InlineData(15, Player1, "LimitlessCreativity")]
		public void ShouldUpdateTheName(int index, EPlayerPosition position, string newName)
		{
			int     pos      = Utils.GetPosition(index, position);
			byte[ ] expected = Utils.ConvertNameToRaw(newName);


			_subject.UpdatePlayerName(index, position, newName);
			_subject.Dispose();
			_subject = null;

			_resultTestFile = new FileStream(_resultTestFilePath, FileMode.Open);
			var buffer = new byte[ 131 ];

			_resultTestFile.Seek(pos, SeekOrigin.Begin);
			_resultTestFile.Read(buffer, 0, 131);

			buffer.Should()
				  .HaveSameCount(expected)
				  .And
				  .BeEquivalentTo(expected);
		}
	}

	internal static class Utils
	{
		internal static byte[ ] ConvertNameToRaw(string name)
		{
			var raw = new byte[ 131 ];

			for (var i = 0; i < 131; i++)
				raw[i] = i < name.Length * 2 && (i + 1) % 2 != 0 ? Convert.ToByte(name[i / 2]) : (byte)0x0;

			return raw;
		}

		internal static int GetPosition(int replayIndex, EPlayerPosition position)
		{
			int replayPosition = KlaK.INITIAL_OFFSET + KlaK.OFFSET_BETWEEN_MATCHES * replayIndex;
			replayPosition += position == Player1 ? 0 : KlaK.OFFSET_BETWEEN_PLAYERS;
			return replayPosition;
		}
	}
}
