﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using DynamicData;
using KLakShared;

namespace KlaKLib
{
	public class KlaK : IDisposable
	{
		public string replayListName = "../../../../02_replay_list.bin.template"; // Replay list filename
		public string resultListName = "../../../../02_replay_list.bin";          // Result list filename

		public const int INITIAL_OFFSET = 0xD8; // Offset from the SOF to the first match first player name
		public const int OFFSET_BETWEEN_PLAYERS = 0xE0; // Offset between Player 1 and Player 2 names
		public const int OFFSET_BETWEEN_MATCHES = 0x2B0; // Offset from one match to the next one
		public const int MAX_REPLAYS = 40; // Max amount of replays to modify/read in the replay list
		public const int PLAYER_NAME_LENGTH = 131; // Max amount of bytes per player name (every other byte is empty)

		private string _replayListPath;
		private string _resultListPath;

		public FileStream file;
		public FileStream resultFile;

		private SourceCache<MatchData, int> _source = new(t => t.Index);
		private MatchData                   _matchData;

		public IObservable<IChangeSet<MatchData, int>> Connect( ) => _source.Connect();

		public KlaK(bool autoOpen = true)
		{
			if (autoOpen) OpenReplayList();
		}

		/// <summary>
		/// Opens replay_list file and copies it to resulting file and checks if its possible to seek
		/// </summary>
		/// <exception cref="IOException">file stream returns false on CanSeek</exception>
		public void OpenReplayList( )
		{
			_replayListPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, replayListName);
			_resultListPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, resultListName);

			file = new FileStream(_replayListPath, FileMode.Open);
			File.Copy(_replayListPath, _resultListPath, true);
			resultFile = new FileStream(_resultListPath, FileMode.OpenOrCreate);
			GetPlayerNamesFromStream(resultFile);
		}

		/// <summary>
		/// Resets the output file
		/// </summary>
		public void ResetFile( )
		{
			resultFile.Dispose();
			File.Copy(_replayListPath, _resultListPath, true);
			resultFile = new FileStream(_resultListPath, FileMode.OpenOrCreate);
			GetPlayerNamesFromStream(resultFile);
		}

		public void SaveFile( ) { resultFile.Flush(); }

		private int GetReplayPosition(int replayIndex, EPlayerPosition position)
		{
			if (replayIndex > MAX_REPLAYS)
				throw new ArgumentException("replayIndex exceeds the maximum amount of replays");
			int replayPosition = INITIAL_OFFSET + OFFSET_BETWEEN_MATCHES * replayIndex;
			replayPosition += position == EPlayerPosition.Player1 ? 0 : OFFSET_BETWEEN_PLAYERS;
			return replayPosition;
		}

		/// <summary>
		/// Moves the caret of the file stream to the specified replay index and player
		/// </summary>
		/// <param name="replayIndex">the index of the replay to seek to</param>
		/// <param name="position">player position either PlayerOne or PlayerTwo</param>
		/// <param name="fileStream">File stream to manipulate</param>
		public void GotoPlayerNames(int replayIndex, EPlayerPosition position, FileStream fileStream)
		{
			int replayPosition = GetReplayPosition(replayIndex, position);
			fileStream.Seek(replayPosition, SeekOrigin.Begin);
		}

		/// <summary>
		/// Processes the raw name in bytes into a string
		/// </summary>
		/// <param name="name">Raw name received from the replay list</param>
		/// <returns>the processed name as a string of correct size</returns>
		private string ProcessRawName(Span<byte> name)
		{
			// Sets the capacity to the known maximum of characters a player can have
			// Divided by 2 to account for the control characters every second character
			var processedStringBuilder = new StringBuilder(PLAYER_NAME_LENGTH / 2);
			// Incremented by 2 to account for and skip control characters
			for (var i = 0; i < name.Length; i += 2)
				// Constrains the name to only append printable ascii characters from 32 to 126
				if (name[i] >= ' ' && name[i] <= '~')
					processedStringBuilder.Append(Convert.ToChar(name[i]));

			return processedStringBuilder.ToString();
		}

		public MatchData GetMatchData(int index)
		{
			var lookup = _source.Lookup(index);
			if (!lookup.HasValue)
				throw new
					ArgumentException($"No Match Data stored for index -> {index}. Ensure that the names were collected.");
			return lookup.Value;
		}

		/// <summary>
		/// Converts given string into a correct binary representation
		/// </summary>
		/// <param name="name">String to convert to binary</param>
		/// <returns>A ReadOnlySpan of bytes representing the string in binary</returns>
		private ReadOnlySpan<byte> ConvertToRaw(string name)
		{
			var raw = new byte[ PLAYER_NAME_LENGTH ];

			for (var i = 0; i < PLAYER_NAME_LENGTH; i++)
				raw[i] = i < name.Length * 2 && (i + 1) % 2 != 0 ? Convert.ToByte(name[i / 2]) : (byte)0x0;

			return raw;
		}

		/// <summary>
		/// Gets all the current names or <paramref name="amount"/> in the replay_list file
		/// </summary>
		/// <param name="fileStream"> file stream to query information from </param>
		/// <param name="amount">The amount of matches to get from the replay list. Capped at <see cref="MAX_REPLAYS"/></param>
		/// <returns> a list of <see cref="MatchData"/> each entry is of a single match</returns>
		public List<MatchData> GetPlayerNamesFromStream(FileStream fileStream, int amount = MAX_REPLAYS)
		{
			// Capping the amount to MAX_REPLAYS
			amount = Math.Min(amount, MAX_REPLAYS);
			List<MatchData> data = new List<MatchData>(amount);

			Span<byte> playerName = new byte[ PLAYER_NAME_LENGTH ];
			for (var i = 0; i < amount; i++)
			{
				GotoPlayerNames(i, EPlayerPosition.Player1, fileStream);
				fileStream.Read(playerName);
				string playerOneName = ProcessRawName(playerName);
				GotoPlayerNames(i, EPlayerPosition.Player2, fileStream);
				fileStream.Read(playerName);
				string playerTwoName = ProcessRawName(playerName);
				_matchData = new MatchData(i, playerOneName, playerTwoName);
				data.Add(_matchData);
			}

			_source.AddOrUpdate(data);

			return data;
		}

		/// <summary>
		/// Updates the File and UI with a new name at a specified match
		/// </summary>
		/// <param name="replayIndex">Match ID to edit the name</param>
		/// <param name="position">New player name position (i.e Player One or Player Two)</param>
		/// <param name="newName">New name of the specified player</param>
		/// <exception cref="ArgumentException">throws ArgumentException if new name exceeds player name limit</exception>
		public void UpdatePlayerName(int replayIndex, EPlayerPosition position, string newName)
		{
			if (newName.Length > PLAYER_NAME_LENGTH / 2)
				throw new ArgumentException($"newName exceeds player name length limit [{PLAYER_NAME_LENGTH / 2}]");
			MatchData data = GetMatchData(replayIndex);
			data.SetPlayerName(position, newName);
			GotoPlayerNames(replayIndex, position, resultFile);
			var rawName = ConvertToRaw(newName);
			resultFile.Write(rawName);
			_source.AddOrUpdate(data);
		}

		public void Dispose( )
		{
			file?.Flush();
			file?.Close();
			file?.Dispose();
			resultFile?.Flush();
			resultFile?.Close();
			resultFile?.Dispose();
			file       = null;
			resultFile = null;
		}
	}
}
